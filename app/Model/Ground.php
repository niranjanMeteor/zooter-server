<?php
App::uses('AppModel', 'Model');
/**
 * Ground Model
 *
 */
class Ground extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
